#!/usr/bin/env pyhton
import __main__, os
from subprocess import call
from sys import exit


# Entry function, checking whether necessary programs are installed
def init(programs_list):
    # Try to run this
    try:
        # Checker is incremented after every successful program check in for loop below.
        checker = 0
        # The amount of programs as elements of the list.
        programs_list_len = len(programs_list)
        # Looping thru programs list
        for program in programs_list:
            # Checking whether the n-th program of the list is currently installed.
            if check_installed(program) != None:
                print('[' + fn + '] ' + '"' + program + '" found: ' +
                      check_installed(program))
                # Program found => increment checker variable.
                checker += 1
            # If program is not installed.
            else:
                print('[' + fn + '] ' + '"' + program + '" missing.')

        # If the checking procedure in the for loop was successful for every program, checker is
        # going to be equal to the amount of elements in "programs_list".
        if checker == programs_list_len:
            print('[' + fn +
                  '] All necessary programs are installed. Continuing...')
            return choose_dir()
        # If checker is less than the amount of elements, error will be displayed.
        # User may continue by decision.
        else:
            print(
                '[' + fn +
                '] Please install the missing program(s) listed above.\nYou may continue executing this script, but there might be some unexpected errors further down the road!'
            )
            decision_continue = input('[' + fn + '] Continue? [y/n]: ')
            if decision_continue == 'y' or decision_continue == 'Y':
                return choose_dir()
            else:
                return exit(0)
    # Simple exception handling,
    except Exception as e:
        return '[' + fn + '] Exception thrown: ' + str(e)


# Not trying to reinvent the wheel, brash copy & paste from stackoverflow.
# Source: https://stackoverflow.com/questions/377017/test-if-executable-exists-in-python
def check_installed(program):
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ['PATH'].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None


# Setting download directory once.
def choose_dir():
    # Try to run this
    try:
        # Prompt for directory.
        path = input('\n[' + fn + '] Choose download directory: ')

        # Do some weird shit for windows path structure support.
        path = os.path.normpath(path)

        # Trying to avoid code repetition.
        # Ignoring [q] and [empty] as inputs regarding path manipulations.
        if path != 'q' and path != '':
            # Tilde replacement for /home/
            if path.split()[0][0] == '~':
                path = os.path.expanduser(path)

            # If given path ends without a forward slash, slash will be appended.
            if not path.endswith('/'):
                path += '/'

        # Exit by hitting [q] and [Enter].
        if path == 'q':
            return exit(0)
        # Empty path given, setting local direcotry for download.
        elif path == '':
            path = os.path.dirname(os.path.realpath(__file__))
            print('[' + fn + '] Download path set to "' + path + '".')
            # Calling main function with "default" path.
            return start(path)
        # If given path does exist.
        elif os.path.exists(path):
            print('[' + fn + '] Download path set to "' + path + '".')
            # Calling main function with chosen path.
            return start(path)
        # User may create his directory of choice while script is running.
        elif not os.path.exists(path):
            # Checking if the path structure above the directory of choice is valid.
            if os.path.exists(os.path.dirname(os.path.dirname(path))):
                # Prompting for user decision.
                decision_create = input('[' + fn +
                                        '] Create unknown directory "' + path +
                                        '"? [y/n]: ')
                # Checking the decision
                if decision_create == 'y' or decision_create == 'Y':
                    # Create the directory (e.g. "path")
                    os.makedirs(path)
                    print('[' + fn + '] Download path created and set to "' +
                          path + '".')
                    # Calling main function with newly created path.
                    return start(path)
                else:
                    print('[' + fn + '] Please choose an exisitng directory.')
                    # Return to start.
                    return choose_dir()
            else:
                print('[' + fn + '] Unknown directory. Please try again!')
                # Return to start.
                return choose_dir()
        # If path is not found.
        else:
            print('[' + fn + '] Unknown directory. Please try again!')
            # Return to start.
            return choose_dir()
    # Simple exception handling,
    except Exception as e:
        return '[' + fn + '] Exception thrown: ' + str(e)


# Main function
def start(path):
    # Try to run this
    try:
        # Prompt
        target_url = input('\n[' + fn + '] Music Source (URL/Playlist etc.): ')

        # Exit by hitting [q] and [Enter].
        if target_url == 'q':
            return exit(0)
        # Checking whether the provided string is separated by semicolon.
        # If that's not the case "target_list" will only contain the provided string.
        elif target_url.find(';') == -1 and target_url != '':
            target_list = target_url
            # Going to trigger "[Single File Download Mode]".
            return download(target_list, path)
        # If the provided string is separated, a list object is created containing multiple URLs.
        elif target_url.find(';') != -1 and target_url != '':
            target_list = target_url.split(';')
            # Deleting empty list elements, created by fucking up the separation with semicolons.
            target_list = filter(None, target_list)
            # Going to trigger "[Multi File Download Mode]".
            return download(target_list, path)
        else:
            # For those who like to hit enter without entering anything.
            return start(path)
    # Simple exception handling.
    except Exception as e:
        return '[' + fn + '] Exception thrown: ' + str(e)


def call_proc(target, path):
    # Trying to avoid code repetition.
    # Calling "youtube-dl" as a subprocess in a shell, which then executes the file and thumbnail download.
    filepath = path + '%(title)s.%(ext)s'
    cmd = 'youtube-dl --extract-audio -ciw --audio-format mp3 --audio-quality 320K --prefer-ffmpeg --embed-thumbnail --add-metadata --xattrs ' + '"' + target + '"' + ' -o ' + '"' + filepath + '"'
    return call(cmd, shell=True)


def download(target_list, path):
    try:
        # Checking wether "target_list" is really a list.
        # If it's a string, then there's only one element to download.
        if type(target_list) is str:
            print('[' + fn + '] [Single File Download Mode] Please wait...')
            call_proc(target_list, path)
            print('[' + fn + '] Download completed!')
            return start(path)
        # If it's really a list, then there are multiple elements to download.
        elif type(target_list) is list:
            # Looping thru the list. Each time the body of the loop is entered, a new download cycle is kicked of.
            for target_list_entry in target_list:
                print(
                    '[' + fn +
                    '] [Multi File Download Mode] Please wait, download cycle begins...'
                )
                call_proc(target_list_entry, path)
                print('[' + fn + '] Current download cycle completed!')
            # Loop finished executing, downloads completed.
            print('[' + fn + '] All downloads completed!')
            # Go back to new prompt.
            return start(path)
        # Never gonna reach this part.
        else:
            return start(path)
    except Exception as e:
        return '[' + fn + '] Exception thrown: ' + str(e)


############################ Where shit is getting real ############################

global fn

# Get filename of script.
fn = __main__.__file__

# Script version header.
version = fn + ' (v. 2015.10.02)'

# List of required programs, version and download source
programs = [['python', '2.7.9', 'https://www.python.org/downloads/'],
            [
                'youtube-dl', '2015.09.28',
                'https://rg3.github.io/youtube-dl/download.html'
            ], ['ffmpeg', '2.8', 'https://www.ffmpeg.org/download.html'],
            [
                'attr', '2.4.39-1',
                'http://download.savannah.gnu.org/releases/attr/'
            ]]

programs_list = []

print('# [' + version + ']\n')

# List of required programs, only printed once
enum = 1
for element in programs:
    print('# ' + str(enum) + '. Install ' + element[0] + ' >= ' + element[1] +
          ' (' + element[2] + ').')
    programs_list.extend([element[0]])
    enum += 1
print('\r')

# Entry point
print(init(programs_list))
